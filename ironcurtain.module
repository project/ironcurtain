<?php
/**
 * @file
 * ironcurtain.module
 */

use M6Web\Component\Firewall\Firewall;

/**
 * Implements hook_menu().
 */
function ironcurtain_menu() {
  $items['admin/config/system/ironcurtain'] = array(
    'title' => 'Iron Curtain',
    'description' => 'Configure Iron Curtain settings',
    'access arguments' => array('administer iron curtain settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ironcurtain_settings_form'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ironcurtain.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function ironcurtain_permission() {
  return array(
    'administer iron curtain settings' => array(
      'title' => t('Administer Iron Curtain settings'),
      'description' => t('Administer Iron Curtain settings'),
    ),
    'bypass iron curtain restrictions' => array(
      'title' => t('Bypass Iron Curtain restrictions'),
      'description' => t('Bypass Iron Curtain restrictions'),
    ),
  );
}

/**
 * Implements hook_boot().
 */
function ironcurtain_boot() {
  // Return if no restrictions are set.
  $unrestricted_paths = variable_get('ironcurtain_unrestricted_paths', array());
  $blocked_paths = variable_get('ironcurtain_blocked_paths', array());
  $restricted_paths = variable_get('ironcurtain_restricted_paths', array());
  if (empty($unrestricted_paths) && empty($blocked_paths) && empty($restricted_paths)) {
    return;
  }

  // Check permission to bypass.
  if (variable_get('ironcurtain_permission_override', TRUE)) {
    drupal_load('module', 'user');
    if (user_access('bypass iron curtain restrictions')) {
      return;
    }
  }

  // Prepare to check if the path is restricted.
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $path = current_path();

  // Check for unrestricted path.
  if (!empty($unrestricted_paths)) {
    if (drupal_match_path(current_path(), $unrestricted_paths)) {
      return;
    }
  }

  $user_ip = ip_address();
  $logging = variable_get('ironcurtain_logging', FALSE);
  $fast_denied = variable_get('ironcurtain_fast_denied_page', FALSE);

  if (!empty($blocked_paths)) {
    if (drupal_match_path(current_path(), $blocked_paths)) {
      $ip_blacklist = variable_get('ironcurtain_ip_blacklist', array());
      $firewall = new Firewall();
      $allowed = $firewall
        ->setDefaultState(TRUE)
        ->addList($ip_blacklist, 'blacklist', FALSE)
        ->setIpAddress($user_ip)
        ->handle();
      if (!$allowed) {
        if ($logging) {
          watchdog('ironcurtain', 'Access denied to blocked path !path from blacklisted IP !ip', array(
            '!path' => current_path(),
            '!ip' => $user_ip,
          ), WATCHDOG_NOTICE);
        }
        if ($fast_denied) {
          header($_SERVER['SERVER_PROTOCOL'] . " 403 Forbiden");
          print '<h1>Access denied</h1>';
        }
        else {
          drupal_access_denied();
        }
        drupal_exit();
      }
      return;
    }
  }

  if (!empty($restricted_paths)) {
    if (drupal_match_path(current_path(), $restricted_paths)) {
      $ip_whitelist = variable_get('ironcurtain_ip_whitelist', array());
      $firewall = new Firewall();
      $allowed = $firewall
        ->setDefaultState(FALSE)
        ->addList($ip_whitelist, 'whitelist', TRUE)
        ->setIpAddress($user_ip)
        ->handle();
      if (!$allowed) {
        if ($logging) {
          watchdog('ironcurtain', 'Access denied to restricted path !path from blacklisted IP !ip', array(
            '!path' => current_path(),
            '!ip' => $user_ip,
          ), WATCHDOG_NOTICE);
        }
        if ($fast_denied) {
          header($_SERVER['SERVER_PROTOCOL'] . " 403 Forbiden");
          print '<h1>Access denied</h1>';
        }
        else {
          drupal_access_denied();
        }
        drupal_exit();
      }
      return;
    }
  }
}
